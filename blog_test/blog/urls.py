from django.urls import path
from . import views

app_name = 'blog'


urlpatterns = [
    path('', views.home, name='blog-home'),
     path('about/', views.about, name='blog-about'),
    path('resume/', views.resume, name='blog-resume'),
    path('blog_post/', views.PostList, name='blog-blg_post'),
    path('blog_detail/<int:pk>', views.PostDetail, name='blog-post_detail'),
]
