# Generated by Django 4.2.2 on 2023-07-17 09:12

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("blog", "0008_delete_aboutpage"),
    ]

    operations = [
        migrations.CreateModel(
            name="AboutPage",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("birthday", models.DateField(default=0)),
                ("address", models.TextField(max_length=200)),
                ("number", models.TextField(max_length=20)),
                ("website", models.TextField(max_length=20)),
                ("age", models.PositiveIntegerField(default=0)),
                ("email", models.EmailField(max_length=254)),
                ("degree", models.TextField(max_length=254)),
                ("content", models.TextField(max_length=5000000)),
                ("online", models.TextField(max_length=500000)),
                ("my_current_research", models.TextField(max_length=500000)),
                ("my_current_research1", models.TextField(max_length=500000)),
                ("my_current_research2", models.TextField(max_length=500000)),
                ("my_current_research3", models.TextField(max_length=500000)),
                ("my_current_research4", models.TextField(max_length=500000)),
                ("my_future_goals", models.TextField(max_length=500000)),
                ("my_future_goals1", models.TextField(max_length=500000)),
                ("my_future_goals2", models.TextField(max_length=500000)),
                ("my_future_goals3", models.TextField(max_length=500000)),
                ("my_future_goals4", models.TextField(max_length=500000)),
                ("my_mission", models.TextField(max_length=500000)),
                ("my_mission1", models.TextField(max_length=500000)),
                ("my_mission2", models.TextField(max_length=500000)),
                ("my_mission3", models.TextField(max_length=500000)),
                ("my_mission4", models.TextField(max_length=500000)),
                ("my_vision", models.TextField(max_length=500000)),
                ("my_vision1", models.TextField(max_length=500000)),
                ("my_vision2", models.TextField(max_length=500000)),
                ("my_vision3", models.TextField(max_length=500000)),
                ("my_vision4", models.TextField(max_length=500000)),
                ("my_quotes", models.TextField(max_length=500000)),
                ("my_quotes1", models.TextField(max_length=500000)),
                ("my_quotes2", models.TextField(max_length=500000)),
                ("my_quotes3", models.TextField(max_length=500000)),
                ("my_quotes4", models.TextField(max_length=500000)),
            ],
        ),
    ]
