# Generated by Django 4.2.2 on 2023-06-27 09:50

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("blog", "0002_interests_skills_workstats_alter_aboutpage_degree_and_more"),
    ]

    operations = [
        migrations.CreateModel(
            name="ResumePage1",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("Summary", models.CharField(max_length=200)),
                ("aboutme", models.CharField(max_length=2000)),
                ("Address", models.CharField(max_length=2000)),
                ("phone", models.CharField(max_length=2000)),
                ("email", models.CharField(max_length=2000)),
                ("education", models.CharField(max_length=200)),
                ("education_head", models.CharField(max_length=200)),
                ("education_1st_year", models.CharField(max_length=200)),
                ("education_2nd_year", models.CharField(max_length=200)),
                ("education_1st_year_university", models.CharField(max_length=200)),
                ("education_2nd_year_university", models.CharField(max_length=200)),
                ("education_1st_year_briefintro", models.CharField(max_length=2000)),
                ("education_2nd_year_briefintro", models.CharField(max_length=2000)),
                ("experience_head", models.CharField(max_length=200)),
                ("experience_position1", models.CharField(max_length=200)),
                ("experience_position1_year", models.CharField(max_length=200)),
                ("experience_position2", models.CharField(max_length=200)),
                ("experience_position2_year", models.CharField(max_length=200)),
                ("experience_position3", models.CharField(max_length=200)),
                ("experience_position3_year", models.CharField(max_length=200)),
                ("experience_position1_location", models.CharField(max_length=2000)),
                ("experience_position2_location", models.CharField(max_length=2000)),
                ("experience_position3_location", models.CharField(max_length=2000)),
                ("exp1_brief_dscp1", models.CharField(max_length=2000)),
                ("exp1_brief_dscp2", models.CharField(max_length=2000)),
                ("exp1_brief_dscp3", models.CharField(max_length=2000)),
                ("exp2_brief_dscp1", models.CharField(max_length=2000)),
                ("exp2_brief_dscp2", models.CharField(max_length=2000)),
                ("exp2_brief_dscp3", models.CharField(max_length=2000)),
                ("exp3_brief_dscp1", models.CharField(max_length=2000)),
                ("exp3_brief_dscp2", models.CharField(max_length=2000)),
                ("exp3_brief_dscp3", models.CharField(max_length=2000)),
            ],
        ),
        migrations.DeleteModel(
            name="ResumePage",
        ),
    ]
