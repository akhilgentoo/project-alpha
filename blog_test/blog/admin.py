from django.contrib import admin
from .models import *


def get_actions(self, request):
    actions = super().get_actions(request)
    if 'delete_selected' in actions:
        del actions['delete_selected']
    return actions

class aboutadmin(admin.ModelAdmin):
	list_display=["birthday",
    "address",
    "number",
    "website",
    "age",
    "email",
    "degree",
    "content",
    "online",
    "my_current_research",
    "my_current_research1",
    "my_current_research2",
    "my_current_research3",
    "my_current_research4",
    "my_future_goals",
    "my_future_goals1",
    "my_future_goals2",
    "my_future_goals3",
    "my_future_goals4",
    "my_mission",
    "my_mission1",
    "my_mission2",
    "my_mission3",
    "my_mission4",
    "my_vision",
    "my_vision1",
    "my_vision2",
    "my_vision3",
    "my_vision4",
    "my_quotes",
    "my_quotes1",
    "my_quotes2",
    "my_quotes3",
    "my_quotes4"

]

class interestsadmin(admin.ModelAdmin):
	list_display=["interest1","interest2","interest3","interest4","interest5","interest6","interest7","interest8"]

class skillsadmin(admin.ModelAdmin):
	list_display=["skill1","skill2","skill3","skill4","skill5","skill6","skill7","skill8"]

class workstatsadmin(admin.ModelAdmin):
	list_display=["work1","work2","work3","work4"]

class ResumePage1admin(admin.ModelAdmin):
	list_display=  ["Summary",
    "aboutme",
    "Address",
    "phone",
    "email",
    "education",
    "education_head",
    "education_1st_year",
    "education_2nd_year",
    "education_1st_year_university",
    "education_2nd_year_university",
    "education_1st_year_briefintro",
    "education_2nd_year_briefintro",
    "experience_head",
    "experience_position1",
    "experience_position1_year",
    "experience_position2",
    "experience_position2_year",
    "experience_position3",
    "experience_position3_year",
    "experience_position1_location",
    "experience_position2_location",
    "experience_position3_location",
    "exp1_brief_dscp1", 
    "exp1_brief_dscp2", 
    "exp1_brief_dscp3", 
    "exp2_brief_dscp1",
    "exp2_brief_dscp2", 
    "exp2_brief_dscp3", 
    "exp3_brief_dscp1", 
    "exp3_brief_dscp2", 
    "exp3_brief_dscp3" ]

class blg_postadmin(admin.ModelAdmin):
    list_display=["subject","date","topic","reference_to","create_by"]

admin.site.register(AboutPage,aboutadmin)
admin.site.register(ResumePage1,ResumePage1admin)
admin.site.register(interests,interestsadmin)
admin.site.register(skills,skillsadmin)
admin.site.register(workstats,workstatsadmin)
admin.site.register(blg_post,blg_postadmin)

