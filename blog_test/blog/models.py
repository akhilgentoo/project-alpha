from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class AboutPage(models.Model):
    birthday = models.DateField(default=0)
    address = models.TextField(max_length=200)
    number = models.TextField(max_length=20)
    website = models.TextField(max_length=20)
    age = models.PositiveIntegerField(default=0)  # Added a default value for the age field
    email = models.EmailField(max_length=254)
    degree = models.TextField(max_length=254)
    content = models.TextField(max_length=5000000)
    online = models.TextField(max_length=500000)
    my_current_research = models.TextField(max_length=500000)
    my_current_research1 = models.TextField(max_length=500000)
    my_current_research2 = models.TextField(max_length=500000)
    my_current_research3 = models.TextField(max_length=500000)
    my_current_research4 = models.TextField(max_length=500000)
    my_future_goals = models.TextField(max_length=500000)
    my_future_goals1 = models.TextField(max_length=500000)
    my_future_goals2 = models.TextField(max_length=500000)
    my_future_goals3 = models.TextField(max_length=500000)
    my_future_goals4 = models.TextField(max_length=500000)
    my_mission= models.TextField(max_length=500000)
    my_mission1= models.TextField(max_length=500000)
    my_mission2= models.TextField(max_length=500000)
    my_mission3= models.TextField(max_length=500000)
    my_mission4= models.TextField(max_length=500000)
    my_vision= models.TextField(max_length=500000)
    my_vision1= models.TextField(max_length=500000)
    my_vision2= models.TextField(max_length=500000)
    my_vision3= models.TextField(max_length=500000)
    my_vision4= models.TextField(max_length=500000)
    my_quotes= models.TextField(max_length=500000)
    my_quotes1= models.TextField(max_length=500000)
    my_quotes2= models.TextField(max_length=500000)
    my_quotes3= models.TextField(max_length=500000)
    my_quotes4= models.TextField(max_length=500000)

    def __str__(self):
        
        return self.email  


class interests(models.Model):
    interest1 = models.TextField(max_length=500000)
    interest2 = models.TextField(max_length=500000)
    interest3 = models.TextField(max_length=500000)
    interest4 = models.TextField(max_length=500000)
    interest5 = models.TextField(max_length=500000)
    interest6 = models.TextField(max_length=500000)
    interest7 = models.TextField(max_length=500000)
    interest8 = models.TextField(max_length=500000)    

    def __str__(self):
        return self.interest1

class skills(models.Model):
    skill1 = models.TextField(max_length=500000)
    skill2 = models.TextField(max_length=500000)
    skill3 = models.TextField(max_length=500000)
    skill4 = models.TextField(max_length=500000)
    skill5 = models.TextField(max_length=500000)
    skill6 = models.TextField(max_length=500000)
    skill7 = models.TextField(max_length=500000)
    skill8 = models.TextField(max_length=500000)    

    def __str__(self):
        return self.skill1

class workstats(models.Model):
    work1 = models.TextField(max_length=500000)
    work2 = models.TextField(max_length=500000)
    work3 = models.TextField(max_length=500000)
    work4 = models.TextField(max_length=500000)


    def __str__(self):
        return self.work1


class ResumePage1(models.Model):
    Summary = models.TextField(max_length=200)
    aboutme = models.TextField(max_length=500000)
    Address = models.TextField(max_length=500000)
    phone = models.TextField(max_length=500000)
    email = models.TextField(max_length=500000)
    education = models.TextField(max_length=200)
    education_head = models.TextField(max_length=200)
    education_1st_year = models.TextField(max_length=200)
    education_2nd_year = models.TextField(max_length=200)
    education_1st_year_university = models.TextField(max_length=200)
    education_2nd_year_university = models.TextField(max_length=200)
    education_1st_year_briefintro = models.TextField(max_length=500000)
    education_2nd_year_briefintro = models.TextField(max_length=500000)
    experience_head = models.TextField(max_length=200)
    experience_position1 = models.TextField(max_length=200)
    experience_position1_year = models.TextField(max_length=200)
    experience_position2 = models.TextField(max_length=200)
    experience_position2_year = models.TextField(max_length=200)
    experience_position3 = models.TextField(max_length=200)
    experience_position3_year = models.TextField(max_length=200)
    experience_position1_location = models.TextField(max_length=500000)
    experience_position2_location = models.TextField(max_length=500000)
    experience_position3_location = models.TextField(max_length=500000)
    exp1_brief_dscp1= models.TextField(max_length=500000)
    exp1_brief_dscp2= models.TextField(max_length=500000)
    exp1_brief_dscp3= models.TextField(max_length=500000)
    exp2_brief_dscp1= models.TextField(max_length=500000)
    exp2_brief_dscp2= models.TextField(max_length=500000)
    exp2_brief_dscp3= models.TextField(max_length=500000)
    exp3_brief_dscp1= models.TextField(max_length=500000)
    exp3_brief_dscp2= models.TextField(max_length=500000)
    exp3_brief_dscp3= models.TextField(max_length=500000)
    def __str__(self):
        return self.phone

STATUS = (
    (0,"Draft"),
    (1,"Publish")
)
class blg_post(models.Model):
    subject = models.TextField(max_length=200)
    date = models.DateField()
    topic = models.TextField(max_length=500000)
    reference_to = models.TextField(max_length=200)
    create_by = models.TextField(max_length=200)
    status = models.IntegerField(choices=STATUS, default=0)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.create_by

