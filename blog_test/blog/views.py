from django.shortcuts import render , get_object_or_404
from django.http import HttpResponse
from django.views import generic
'''from .models import skills,AboutPage,workstats,interests,ResumePage1,blg_post'''
from .models import *

def home(request):
    return render(request, 'blog/home.html')


'''def about(request):
    post = AboutPage.objects.all()
    return render(request, 'blog/about.html', {'post': post} )'''

def about(request):
    post = AboutPage.objects.all()
    work = workstats.objects.all()
    skill = skills.objects.all()
    interest = interests.objects.all()
    return render(request,'blog/about.html',{'post': post ,'work': work,'skill':skill,'interest':interest})




def resume(request):
    ResumePage = ResumePage1.objects.all()
    return render(request, 'blog/resume.html',{'ResumePage': ResumePage })


def PostList(request):
    '''queryset = blg_post.objects.filter(status=1).order_by('-created_on')'''
    queryset = blg_post.objects.all()
    return render(request, 'blog/blg_posts.html',{'blg_post': queryset })


def PostDetail(request, pk):
    queryset = get_object_or_404(blg_post, pk=pk)
    return render(request, 'blog/post_detail.html',{'blg_post': queryset })

'''class PostList(generic.ListView):
    queryset = blg_post.objects.filter(status=1).order_by('-created_on')
    template_name = 'blog/blg_posts.html'

class PostDetail(generic.DetailView):
    model = blg_post
    template_name = 'blog/post_detail.html'  '''
 